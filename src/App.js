import React from "react";
import "./App.css";
import "./box.css";

class App extends React.Component {
  state = {
    todoList: [], //menyimpan hasil inputan
    inputValue: "" //menulis inputan
  };

  changeHandler = event => {
    //membuat perubahan
    const { value } = event.target;
    this.setState({
      inputValue: value //bukan pakai todoList karena mau mengambil inputan yg diisikan oleh user
    });
  };

  clickHandler = (event, data) => {
    //memberi event jika di klik
    event.preventDefault(); //secara default submit di HTML jika diklik akan mereload halaman. kita gak butuh reload halaman. sehingga pakai preventDefault()
    console.log(event);
    this.setState({
      todoList: [...this.state.todoList, data], //cara baca. todolist mohon copy dan gabungkan isimu dengan data. data yg dimaksud disini adalah value dari inputValue
      inputValue: "" //untuk menghapus input setelah tombol diklik
    });
    // if (event.which === 13) {
    //   console.log(event);
    //   this.setState({
    //     todoList: [...this.state.todoList, data],
    //     inputValue: ""
    //   });
    // }
  };
  enterKeyPress = (event, data) => {
    event.preventDefault();
    //if (event.which === 13) {
    console.log(event);
    this.setState({
      todoList: [...this.state.todoList, data],
      inputValue: ""
    });
    //}
  };

  clickDeleteHandler = valueList => {
    const newTodoList = this.state.todoList.filter(list => list !== valueList); // jika nilai list tidak sama dengan nilai valueList maka akan menampilkan semua, kecuali nilai list yg sama dengan nilai valueList.
    this.setState({
      //mengubah nilai todoList menjadi nilai newTodoList. istilahnya ini mereplace todoList lama dengan newTodoList
      todoList: newTodoList
    });
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div>
            <label>Todo List</label>
            <br />
            <input
              type="text"
              value={this.state.inputValue}
              name="inputValue"
              onChange={this.changeHandler}
              className="custom-input"
            />
            <br />
            <button
              className="custom-button"
              onClick={event => this.clickHandler(event, this.state.inputValue)} //berfungsi untuk memparsing nilai event. nilai event dikirim ke clickHandler. selain itu untuk membuat supaya bisa membuat key uniq
            >
              Add Todo List
            </button>
          </div>
          <div className="box-container">
            {this.state.todoList.map((list, index) => {
              //nilai dari list diambil dari inputan
              return (
                //key={index+list} berfungsi untuk membuat key supaya uniq. karena tiap child harus uniq dari child lain
                <div key={index + list} className="box-content">
                  <p>{list}</p>
                  <button onClick={() => this.clickDeleteHandler(list)}>
                    x
                  </button>
                  {/*pakai arrow function bisa mengirim nilai. nilai yg dikirim adalah list*/}
                </div>
              );
            })}
          </div>
        </header>
      </div>
    );
  }
}

export default App;
